#!/usr/bin/env python3

# Copyright (c) 2021 Dominic Adam Walters

import os
import stat


def make_executable(file_name):
    """
    Make a given file executable

    Parameters
    ----------
    file_name : str
        The file to make executable
    """
    st = os.stat(file_name)
    os.chmod(file_name, st.st_mode | stat.S_IEXEC)
