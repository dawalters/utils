#!/usr/bin/env python3

# Copyright (c) 2021 Dominic Adam Walters

from functools import reduce


def camel_case_to_snake_case(s):
    """
    Convert a CamelCase string to snake_case

    Parameters
    ----------
    s : str
        The string to convert

    Returns
    -------
    str
        The snake case representation of the camel case input

    Examples
    --------
    >>> camel_case_to_snake_case("CamelCase")
    'camel_case'
    """
    ret = ""
    for i, char in enumerate(s):
        if i == 0:
            ret += char
        elif i < len(s) - 1:
            last_char = s[i - 1]
            next_char = s[i + 1]
            if char.isupper():
                if next_char.islower():
                    ret += "_"
                ret += char
            else:
                ret += char
                if next_char.isupper():
                    ret += "_"
        else:
            ret += char
    return ret.lower()
