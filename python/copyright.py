#!/usr/bin/env python3

# Copyright (c) 2021 Dominic Adam Walters

import argparse
from datetime import date
import jinja2
import sys


COPYRIGHT_TEMPLATE = "Copyright (c) {{ year }} Dominic Adam Walters"


def generate_copyright_message(year=date.today().year):
    """
    Return a copyright message containing the year specified

    Parameters
    ----------
    year : int
        The year to put into the copyright message. The default is the value
        of date.today().year

    Returns
    -------
    str
        The copyright message

    Examples
    --------
    >>> generate_copyright_message(2021)
    'Copyright (c) 2021 Dominic Adam Walters'
    """
    tm = jinja2.Template(COPYRIGHT_TEMPLATE)
    return tm.render(year=year)


def main(args):
    """
    Write a copyright message to stdout, or append it to a file depending on
    the arguments given

    Parameters
    ----------
    args : Namespace
        A Namespace created by the argparse module. This must contain entries
        for `file` and `year`
    """
    copyright_message = f"{generate_copyright_message(args.year)}\n"
    if args.file is None:
        sys.stdout.write(copyright_message)
    else:
        with open(args.file, "a") as f:
            f.write(copyright_message)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Create copyright messages")
    parser.add_argument("-f", "--file", type=str, default=None,
                        help="Choose a file to append the copyright message "
                             "to")
    parser.add_argument("-y", "--year", type=int, default=date.today().year,
                        help="Choose a year to include in the copyright "
                             f"message (defaults to {date.today().year})")
    args = parser.parse_args()
    main(args)
