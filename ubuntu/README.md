# Ubuntu OpenCPI VM

This directory contains the `setup.sh` script, which configures a stock Ubuntu
18.04 VM for OpenCPI development.

To use this script you will either need `git` or `curl`.

If you have `git`:

```bash
git clone https://gitlab.com/d9t2/utils.git /tmp/utils
/tmp/utils/ubuntu/setup.sh
```

If you have `curl`:

```bash
curl https://gitlab.com/d9t2/utils/-/raw/master/ubuntu/setup.sh > \
    /tmp/utils/ubuntu/setup.sh
/tmp/utils/ubuntu/setup.sh
```
