# Installing and Configuring a CentOS 7 VM for VirtualBox

This is a guide on configuring a VirtualBox VM for CentOS7 for OpenCPI/Xilinx
development.

## Table of Contents

1. [Creating a VM in Virtualbox](#vmcreation)
1. [Installing CentOS7](#vminstallos)
1. [Configuring CentOS7](#vmconfigureos)
1. [Configuring OpenCPI and Xilinx](#vmconfigureopencpi)

## Creating a VM in VirtualBox <a name="vmcreation" />

1. Click the "New" button.
1. Fill in the name, location, system type, and os type. Click "Next".
   ![Name and OS](images/name_and_os.png)
1. Choose an amount of memory to give to the VM. I recommend the highest value
   that is within the green bar. Over allocating at this stage won't cause a
   problem, but will need to be addressed later. Click "Next".
   ![Memory](images/memory.png)
1. Leave "Create a virtual hard disk now" selected. Click "Create".
   ![New Hard Disk](images/new_hard_disk.png)
1. Select "Virtual Hard Disk" and click "Next".
   ![New Virtual Hard Disk](images/new_virtual_hard_disk.png)
1. Leave "Dynamic Allocation" selected. Click "Next".
   ![Dynamic Allocation](images/dynamic_allocation.png)
1. Leave the disk with its default name, and give it a good chunk of memory.
   Ideally this memory will be enough for the `/`, `/home`, and swap
   partitions. I selected 200GB, which should be enough for most use cases.
   Dedicated hard disks can always be created when additional space is needed.
   Existing disks can also be resized, although this isn't as easy. Click
   "Next".
   ![Disk Size](images/disk_size.png)
1. The wizard should have closed, and you will now be able to see an overview
   of the VM that you created. Click the "Settings" button.
   ![Overview before Configuration](images/overview_before_config.png)
1. On the "General" page, go to the "Advanced" sub tab, and change "Shared
   Clipboard" and "Drag'n'Drop" to "Bidirectional".
   ![Shared Clipboard](images/shared_clipboard.png)
1. On the "System" page, go to the "Processor" sub tab, and allocate as many
   processors as possible whilst remaining in the green bar.
   ![Processors](images/cpus.png)
1. If you allocated too much RAM earlier on, you will notice a message saying
   "Invalid settings detected". Reduce your RAM usage by as much as is
   necessary to get this message to stop.
   ![Bad Memory Configuration](images/bad_memory.png)
1. On the "Network" page, click the "Advanced" drop down on Adapter 1. This
   will show you a MAC address. You may need to change this to allow any
   license keys you have to work.
   ![MAC Address](images/mac_address.png)
1. On the "USB" page, select the "USB 3.0 (xHCI) Controller". Finally, click
   "OK".
   ![USB Controller](images/usb_controller.png)

## Installing CentOS7 on a VirtualBox VM <a name="vminstallos" />

1. Click "Start".
1. Your VM will open in a new window, and you'll be presented with a list of
   disks to use to install an operating system onto your VM.
   ![Select ISO](images/select_iso.png)
1. If the CentOS .iso is available in this list, select it and continue.
   Otherwise, click the file icon. In the window that opened click "Add" and
   locate your disk". Click "Choose" once you have made your selection.
   ![CentOS ISO Disk](images/centos_disk.png)
1. You should be greeted by the CentOS install disk grub prompt. Select
   "Install CentOS 7" and hit the "Enter" key (WARNING: The selected item on
   this screen will be in white).
   ![CentOS Install GRUB](images/centos_install_grub.png)
   Note: At this point, clicking in the VM will lock your mouse to the VM
         screen. To escape back to the host OS, press right ctrl.
1. A variety of screens will pop up, and the window may resize several times.
   Eventually you will get to the languages screen. Select English (UK) and
   "Continue".
   ![Language Selection](images/language_selection.png)
1. Click "Software Selection", select "GNOME Desktop", and then click "Done" in
   the upper left.
   ![Software Selection](images/software_selection.png)
1. Click "Network" and enable your network connection (if you have one).
   ![Software Selection](images/network.png)
1. Click "Security Policy" and turn it "Off".
   ![Software Selection](images/security_policy.png)
1. Click "Begin Installation".
1. Click "Root Password", and set the password to "password" (our use case
   doesn't care about the security of the VM, set it to something you can
   type quickly). Click "Done" twice.
   ![Software Selection](images/root_password.png)
1. Click "User Creation" and create a user with name "OpenCPI", username
   "opencpi", and password "password". Make this user administrator. Click
   "Done" twice.
   ![Software Selection](images/user_creation.png)
1. Wait for the installation to finish. Once its done, click the "Reboot"
   button.

## Configuring CentOS7 <a name="vmconfigureos" />

1. Once the VM has rebooted, you will be greeted with the installer again
   asking you to agree to the license terms. Agree, and "Finish Configuration".
1. Login to your user. At this point you can go into the "View" options in
   VirtualBox and use full screen mode.
1. A window will pop up where you can set up languages, keyboard, turn off
   location services, and skip account sign in.
1. First things you'll notice are your mouse is way too sensitive, and the
   screen is tiny.
1. Open the settings menu by clicking the spanner/screwdriver icon in the upper
   left corner menu.
   ![CentOS Settings](images/centos_settings.png)
1. Scroll down to "Devices", and then "Display". Make sure you are in full
   screen, then select a resolution that is below your monitor resolution.
   We will come back later and hopefully make it completely full screen, but
   need the VirtualBox additions disk for that.
   ![Display Resolution](images/resolution.png)
1. Go to Mouse and set the sensitivity to a comfortable level. You may notice
   that the mouse disappears when moved quickly. We should be able to fix this
   later.
1. Right click, and open a terminal.
   ![Right Click](images/right_click.png)
   Note: If (like me) you prefer dark mode for your terminal, go to "Edit",
         "Profile Preferences", "Colours", untick "Use colours from system
         theme", and select "Solarized Dark".
1. Type `sudo yum update -y` and wait for all of the updates to complete. This
   could take several hours on a poor internet connection. Note: From here
   on, a network connection will be assumed. Whilst this is happening, the
   screen resolution may default back to 800x600. If it does, wait until the
   command completes to change it back.
1. Run the following commands:

   ```bash
   sudo yum install gcc
   sudo yum install "kernel-devel-uname-r == $(uname -r)"
   ```

1. In full screen mode, move your mouse to the bottom of the screen. The
   VirtualBox toolbar should open. Click "Devices", and "Insert guest
   additions CD". When a pop up comes up, allow the disk to run and enter your
   password.
1. Once the install finishes, reboot your VM and log back in. If you toggle
   full screen on and off, you should notice that the VM will now autoresize
   to fill the whole screen. Additionally, copy pasting into the VM should now
   work along with various other features.

## Setting up OpenCPI and Xilinx <a name="vmconfigureopencpi" />

1. Run the following command and wait for the script to finish. This will take
   hours:

   ```bash
   curl https://gitlab.com/d9t2/utils/-/raw/master/centos/install_guide/light_setup.sh > /tmp/light_setup.sh
   chmod +x /tmp/light_setup.sh
   /tmp/light_setup.sh
   ```
