#!/usr/bin/env bash

# Script location and name
me=$(basename "$0")
script_dir=$(cd $(dirname "$0") && pwd)
cd $script_dir

# Check that we have sudo and that we can sudo
if ! command -v sudo; then
    echo "If this is run on a blank CentOS GNOME VM, it may not have sudo "
    echo "installed. Superuser needs to install it:"
    echo "  su"
    echo "  yum install sudo"
    exit 1
fi
check_sudo=$(sudo -v 2>&1)
if [ $? -ne 0 ]; then
    echo "$USER can't sudo. Execute \`usermod -aG wheel $USER\` as su."
    exit 1
fi

# Disable Mouse Acceleration
printf '%s\n%s\n%s\n%s\n%s\n%s\n%s\n' \
    "Section \"InputClass\"" \
    "	Identifier \"My Mouse\"" \
    "	MatchIsPointer \"yes\"" \
    "	Option \"AccelerationNumerator\" \"1\"" \
    "	Option \"AccelerationDenominator\" \"1\"" \
    "	Option \"AccelerationThreshold\" \"0\"" \
    "EndSection" | sudo tee -a /etc/X11/xorg.conf.d/50-mouse-acceleration.conf

# Get necessary packages
sudo yum -y update
# # Git
sudo yum -y install git git-gui
# # GParted for disk management
sudo yum -y install gparted
# # Microsoft Edge for browsing
edge=microsoft-edge-dev-102.0.1220.1-1.x86_64.rpm
wget https://packages.microsoft.com/yumrepos/edge/$edge
sudo yum -y install $edge
rm -f $edge

# Custom Vim install
cd ../
git submodule update --init --recursive
cd programs/vim
./setup.sh
# # Clean Up
rm -rf vim/
cd $script_dir

# Epel
sudo yum -y install epel-release centos-release-scl
sudo yum -y install devtoolset-8
printf "source /opt/rh/devtoolset-8/enable\n" >> $HOME/.bashrc

# Xilinx
xilinx=/opt/Xilinx
sudo mkdir $xilinx
sudo chown -R $USER: $xilinx
mkdir $xilinx/SDK $xilinx/Vitis $xilinx/Vitis_HLS $xilinx/Vivado
mkdir $xilinx/ZynqReleases $xilinx/git
git clone https://github.com/xilinx/linux-xlnx.git $xilinx/git/linux-xlnx
git clone https://github.com/xilinx/u-boot-xlnx.git $xilinx/git/u-boot-xlnx

# OpenCPI
opencpi_loc=/home/$USER/opencpi
git clone https://gitlab.com/opencpi/opencpi.git $opencpi_loc
cd $opencpi_loc
./scripts/install-opencpi.sh
printf "source /home/$USER/opencpi/cdk/opencpi-setup.sh -r\n" >> $HOME/.bashrc

# Reminders to the user
echo ""
echo "setup.sh completed!"
echo ""
echo "Reminder: Setup your git config:"
echo "  \`git config --global user.email <email>\`"
echo "  \`git config --global user.name  <name>\`"
echo ""
echo "Reminder: Get ZynqRelease tarballs and put them in $xilinx/ZynqReleases:"
echo "  \`cd Downloads\`"
echo "  \`mkdir $xilinx/ZynqReleases/2018.3\`"
echo "  \`cp 2018.3-zcu102-release.tar.xz $xilinx/ZynqReleases/2018.3/.\`"
echo "  \`cp 2018.3-zed-release.tar.xz $xilinx/ZynqReleases/2018.3/.\`"
