# CentOS OpenCPI VM

This directory contains the `setup.sh` script, which configures a stock CentOS
7.5 VM for OpenCPI development.

To use this script you will either need `git` or `curl`.

If you have `git`:

```bash
git clone https://gitlab.com/d9t2/utils.git /tmp/utils
/tmp/utils/centos/setup.sh
```

If you have `curl`:

```bash
curl https://gitlab.com/d9t2/utils/-/raw/master/centos/setup.sh > \
    /tmp/utils/centos/setup.sh
/tmp/utils/centos/setup.sh
```
