# Copyright (c) 2021 Dominic Adam Walters

# Processes flags of any of the following forms:
#  * `-y`            - Exposes a variable, yes, with value "1".
#  * `--yes`         - Exposes a variable, yes, with value "1".
#  * `-y=<value>`    - Exposes a variable, yes, with value "<value>".
#  * `--yes=<value>` - Exposes a variable, yes, with value "<value>".
#  * `-y <value>`    - Exposes a variable, yes, with value "<value>".
#  * `--yes <value>` - Exposes a variable, yes, with value "<value>".
# This example would require the sourcing script to define:
#  variables["-y"]="yes"
#  variables["--yes"]="yes"
for arg in "$@"; do
    next_index=$(expr $index + 1)
    if [[ $arg == *"="* ]]; then
        argument_label=${arg%=*}
    else
        argument_label=$arg
    fi
    if [[ -n ${variables[$argument_label]} ]]; then
        if [[ $arg == *"="* ]]; then
            declare ${variables[$argument_label]}=${arg#$argument_label=}
        else
            if [[ $next_index -gt $# ]]; then
                declare ${variables[$argument_label]}=1
            elif [[ ${!next_index} == "-"* ]]; then
                declare ${variables[$argument_label]}=1
            else
                declare ${variables[$argument_label]}=${!next_index}
            fi
        fi
    fi
    index=$index+1
done
