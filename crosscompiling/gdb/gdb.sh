#!/usr/bin/env bash

# Copyright (c) 2021 Dominic Adam Walters

# Environment
script_dir=$(cd $(dirname $0) && pwd)
source ../../settings.sh

# Arguments
source $D9T2_UTILS_ROOT_DIR/scripting/flags/declare_flags.sh
variables["-p"]="platform"
variables["--platform"]="platform"
source $D9T2_UTILS_ROOT_DIR/scripting/flags/prepare_flags.sh
# # Defaults
if [ -z $platform ]; then
    directory=$script_dir/builds/host
    gmp=$D9T2_UTILS_ROOT_DIR/crosscompiling/gmp/builds/host
else
    directory=$script_dir/builds/$platform
    gmp=$D9T2_UTILS_ROOT_DIR/crosscompiling/gmp/builds/$platform
fi

# Clone gdb repository if we don't have one
mkdir -p $script_dir/builds
if [ ! -d $script_dir/builds/src ]; then
    git clone git://sourceware.org/git/binutils-gdb.git $script_dir/builds/src
fi

# Validate $platform
if [ ! -z $platform ]; then
    $D9T2_UTILS_ROOT_DIR/crosscompiling/check_crosscompile_platform.sh \
        --platform $platform
fi

# If gmp isn't built, build it
if [ ! -d $gmp ] || [ ! -d $gmp/build ]; then
    if [ -z $platform ]; then
        $D9T2_UTILS_ROOT_DIR/crosscompiling/gmp/gmp.sh
    else
        $D9T2_UTILS_ROOT_DIR/crosscompiling/gmp/gmp.sh --platform $platform
    fi
fi

# Build
# # Make gdb clone for the platform
if [ ! -d $directory ]; then
    git clone $script_dir/builds/src $directory
fi
prefix=$directory/build
mkdir -p $prefix
# # Configure
cd $directory
if [ -z $platform ]; then
    # # # Compile
    path=$PATH
    ./configure --prefix=$prefix --with-gmp=$gmp/build
else
    # # # Cross Compile
    source $D9T2_UTILS_ROOT_DIR/crosscompiling/toolchains/$platform/setenv.sh
    path=$PATH:$CROSS_COMPILER_ROOT/bin
    PATH=$path \
        CFLAGS="-I$CROSS_COMPILER_ROOT/include -I$gmp/build/include" \
        LDFLAGS="-L$CROSS_COMPILER_ROOT/lib -L$gmp/build/lib" \
            ./configure \
                --build=$($directory/config.guess) --host=$CROSS_COMPILER \
                --prefix=$prefix --with-gmp=$gmp/build
fi
# # Make
# # # Note: make will error when it gets to gdb due to erroneously missing gmp
# # #       We need to separately configure and build it
if PATH=$path make; then
    PATH=$path make install
else
    echo "Expected error, dealing with it..."
    PATH=$path \
        ./configure \
            --build=$($directory/config.guess) --host=$CROSS_COMPILER \
            --prefix=$prefix --with-libgmp-prefix=$gmp/build
    if PATH=$path make; then
        PATH=$path make install
    else
        echo "Build failed"
        exit -1
    fi
fi
