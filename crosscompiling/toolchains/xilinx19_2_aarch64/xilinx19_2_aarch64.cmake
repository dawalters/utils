# Copyright (c) 2021 Dominic Adam Walters
set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_VERSION 4.19.0)

# Compiler
set(PLATFORM xilinx19_2_aarch64)
set(CMAKE_FIND_ROOT_PATH
  /opt/Xilinx/Vitis/2019.2/gnu/aarch64/lin/aarch64-linux
)
set(CMAKE_C_COMPILER aarch64-linux-gnu-gcc)
set(CMAKE_CXX_COMPILER aarch64-linux-gnu-g++)

# Headers and Libraries
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM BOTH)
