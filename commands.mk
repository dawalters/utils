# Copyright (c) 2023 Dominic Adam Walters

at ?= @
dir := $(strip $(shell dirname "$(realpath $(lastword $(MAKEFILE_LIST)))"))

# Admin
sudo ?= $(shell command -v doas || command -v sudo)

# Operating System Package Managers
apt ?= $(shell command -v apt-get)
apt_install := $(apt-get) install -y

pacman ?= $(shell command -v pacman)
pacman_install := $(pacman) -S --noconfirm

yum ?= $(shell command -v yum)
yum_install := $(yum) -y install

# Programming Language Package Managers
cargo ?= $(shell command -v cargo)
cargo_install := $(cargo) install

pip ?= $(shell command -v pip3 || command -v pip)
pip_install := $(pip) install --user

pipx ?= $(shell command -v pipx)
pipx_install := $(pipx) install --user

# Toolchains
cmake ?= $(shell command -v cmake3 || command -v cmake)
