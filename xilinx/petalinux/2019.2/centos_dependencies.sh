#!/usr/bin/env bash

# Copyright (c) 2021 Dominic Adam Walters

# A script to install all Xilinx Petalinux 2019.2 dependencies for CentOS 7

sudo yum install \
    dos2unix iproute gawk gcc gcc-c++ make net-tools ncurses-devel \
    tftp-server zlib-devel openssl-devel flex bison libselinux gnupg wget \
    diffstat chrpath socat xterm autoconf libtool tar unzip texinfo SDL-devel \
    glibc-devel glibc glib2-devel automake screen pax gzip libstdc++
