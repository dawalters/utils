# Visuals

* Styles for various applications

  * https://github.com/stronk-dev/Tokyo-Night-Linux

* Full GTK style:

  * gtk-engine-murrine
  * https://github.com/Fausto-Korpsvart/Tokyo-Night-GTK-Theme
  * /usr/share/themes
  * /usr/share/icons
  * /usr/share/gtksourceview-2.0/styles
