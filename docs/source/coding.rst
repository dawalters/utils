.. MIT License

.. Copyright (c) 2023 Dominic Adam Walters

.. Permission is hereby granted, free of charge, to any person obtaining a copy
.. of this software and associated documentation files (the "Software"), to deal
.. in the Software without restriction, including without limitation the rights
.. to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
.. copies of the Software, and to permit persons to whom the Software is
.. furnished to do so, subject to the following conditions:
.. 
.. The above copyright notice and this permission notice shall be included in all
.. copies or substantial portions of the Software.
.. 
.. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
.. IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
.. FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
.. AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
.. LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
.. OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
.. SOFTWARE.

.. _Coding Practices:

Coding Practices
================

Here are some core concepts that I try to adhere to when writing code.

.. _Coding Don'ts:

Don't
-----

* **Don't** abbreviate names.
* **Don't** use "magic number" literals.
* **Don't** state types in variable names.
* **Don't** name classes "Base" or "Abstract".

  * This is a symptom of bad naming elsewhere.

* **Don't** make modules called "utils".

  * This is a symptom of poor code structure.
  * Move these utilities into their own classes, or add them to current ones.

* **Don't** create nested code.

  * Instead of checking for good conditions first, check bad ones.
  * Once all the bad conditions are eliminated, only the good ones remain.

.. _Coding Dos:

Do
--

* **Do** state units in variable names, unless the type of the variable makes
  the units obvious.
* **Do** write documentation at the same time as you write the code.

  * Make use of generators like sphinx wherever possible, and as early as
    possible.

* **Do** write tests at the same time as you write the code.

  * Code coverage testing is always a good idea.

* **Do** format your code so that changes result in the minimum number of line
  changes in git.
