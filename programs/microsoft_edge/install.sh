#!/usr/bin/env bash

# Copyright (c) 2022 Dominic Adam Walters

# Environment
script_dir=$(cd $(dirname $0) && pwd)

# Install
if command -v apt &> /dev/null; then

  curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > \
      /tmp/microsoft.gpg
  sudo install -o root -g root -m 644 \
    /tmp/microsoft.gpg /etc/apt/trusted.gpg.d/
  sudo sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/edge stable main" > /etc/apt/sources.list.d/microsoft-edge-dev.list'
  sudo rm /tmp/microsoft.gpg
  sudo apt update && sudo apt -y install microsoft-edge-dev

elif command -v pacman &> /dev/null; then

  package=microsoft-edge-stable-bin
  cd $HOME/builds
  aur fetch $package
  cd $HOME/builds/$package
  aur build
  sudo pacman -U $HOME/builds/$package/$package*.pkg.tar.zst

elif command -v yum &> /dev/null; then

  edge=microsoft-edge-dev-102.0.1220.1-1.x86_64.rpm
  wget https://packages.microsoft.com/yumrepos/edge/$edge -P /tmp
  sudo yum -y install /tmp/$edge

fi
