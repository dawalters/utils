# Copyright (c) 2022-2023 Dominic Adam Walters

at ?= @
this_dir := $(strip $(shell dirname "$(realpath $(lastword $(MAKEFILE_LIST)))"))

.PHONY: default
default: install

include $(this_dir)/../../detect-os.mk
include $(this_dir)/../../commands.mk

$(this_dir)/pip.installed:
	$(at)# Check not already installed, then install
	$(at)if command -v pip3 &> /dev/null; then \
		echo "'pip3' already installed."; \
	elif command -v pip &> /dev/null; then \
		echo "'pip' already installed."; \
	elif [ "$(os_name)" = "arch" ]; then \
		$(sudo) $(pacman_install) pip; \
	elif [ "$(os_name)" = "centos" ]; then \
		$(sudo) $(yum_install) python3-pip; \
	elif [ "$(os_name)" = "ubuntu" ]; then \
		$(sudo) $(apt_install) python3-pip; \
	else \
		echo "Operating system '$(os_name)' not supported by 'pip' installer"; \
		exit 1; \
	fi
	$(at)touch $@

.PHONY: install
install: $(this_dir)/pip.installed

.PHONY: clean
clean:
	$(at)rm -f $(this_dir)/pip.installed
