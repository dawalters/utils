# Copyright (c) 2022-2023 Dominic Adam Walters

at ?= @
this_dir := $(strip $(shell dirname "$(realpath $(lastword $(MAKEFILE_LIST)))"))

.PHONY: default
default: install

include $(this_dir)/../../detect-os.mk
include $(this_dir)/../../commands.mk

$(this_dir)/alacritty.installed:
	$(at)# Check not already installed, then get dependencies
	$(at)if command -v alacritty &> /dev/null; then \
		echo "'alacritty' already installed."; \
	elif [ "$(os_name)" = "arch" ]; then \
		$(sudo) $(pacman_install) \
			cmake \
			freetype2 \
			fontconfig \
			pkg-config \
			make \
			libxcb \
			libxkbcommon \
			python; \
	elif [ "$(os_name)" = "centos" ]; then \
		$(sudo) $(yum_install) \
			cmake \
			freetype-devel \
			fontconfig-devel \
			pkgconfig \
			make \
			libxcb-devel \
			libxkbcommon-devel \
			xcb-util-devel \
			python3; \
	elif [ "$(os_name)" = "ubuntu" ]; then \
		$(sudo) $(apt_install) \
			cmake \
			libfreetype6-dev \
			libfontconfig1-dev \
			pkg-config \
			make \
			libxcb-xfixes0-dev \
			libxkbcommon-dev \
			python3; \
	else \
		echo "Operating system '$(os_name)' not supported by 'alacritty' installer"; \
		exit 1; \
	fi
	$(at)# Install
	$(at)if ! command -v alacritty &> /dev/null; then \
		$(at)PKG_CONFIG_PATH=/usr/lib64/pkgconfig \
			$(cargo_install) alacritty; \
	fi
	$(at)touch $@

.PHONY: install
install: $(this_dir)/alacritty.installed

.PHONY: clean
clean:
	$(at)rm -f $(this_dir)/alacritty.installed
