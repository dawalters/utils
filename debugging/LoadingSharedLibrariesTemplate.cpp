// Copyright (c) 2021 Dominic Adam Walters

#include <dlfcn.h>
#include <iostream>

namespace OA = OCPI::API;

bool handle_opening_shared_object(const char* name) {
  void* handle = dlopen(name, RTLD_NOW | RTLD_GLOBAL);
  if (handle == nullptr) {
    std::cout << dlerror() << std::endl;
    return false;
  }
  return true;
}

bool load_libs() {
  bool success = true;
  success = success && handle_opening_shared_object("libname.so");
  return success;
}

int main(/*int argc, char **argv*/) {
  if (!load_libs()) {
    return 1;
  }
  return 0;
}
