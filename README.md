# utils

A selection of utility scripts to do a variety of things.

## Organisation

* centos
  * Utilties to help in setup and configuration of CentOS Operating Systems
* crosscompiling
  * Scripts to crosscompile various programs
* debugging
  * Template debug code
* opendds
  * Information to help with using OpenDDS.
* programs
  * Scripts to compile various programs
* python
  * Python Scripts to do a variety of things
* scripting
  * Utilities to ease the creation of scripts
* ubuntu
  * Utilties to help in setup and configuration of Ubuntu Operating Systems
  * Outdated
